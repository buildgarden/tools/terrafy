// SPDX-FileCopyrightText: 2024 UL Research Institutes
// SPDX-License-Identifier: MIT

package main

import (
	"fmt"
)

type LicenseEyeLicense struct {
	SpdxId  string `yaml:"spdx-id"`
	Content string `yaml:"content"`
}

type LicenseEyeHeader struct {
	Comment     string            `yaml:"comment"`
	License     LicenseEyeLicense `yaml:"license"`
	PathsIgnore []string          `yaml:"paths-ignore,omitempty"`
}

type LicenseEyeConfig struct {
	Header []LicenseEyeHeader `yaml:"header"`
}

func writeLicenseEyeConfig(filename string) {
	config := LicenseEyeConfig{
		Header: []LicenseEyeHeader{
			{
				Comment: "never",
				License: LicenseEyeLicense{
					SpdxId:  license,
					Content: fmt.Sprintf("SPDX-FileCopyrightText: %s\nSPDX-License-Identifier: %s", copyright, license),
				},
				PathsIgnore: []string{
					"**/.terraform/",
					"**/.terraform.lock.hcl",
					"**/*.tfstate*",
					"**/*.tfvars*",
					"**/*.md",
					"**/*.json",
					"LICENSE",
					"NOTICE",
					".secrets.baseline",
				},
			},
		},
	}

	writeFile(filename, encodeYaml(config))
}
