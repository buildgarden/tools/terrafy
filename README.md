# tofufy

<!-- BADGIE TIME -->

[![pipeline status](https://img.shields.io/gitlab/pipeline-status/buildgarden/tools/tofufy?branch=main)](https://gitlab.com/buildgarden/tools/tofufy/-/commits/main)
[![latest release](https://img.shields.io/gitlab/v/release/buildgarden/tools/tofufy)](https://gitlab.com/buildgarden/tools/tofufy/-/releases)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit)](https://github.com/pre-commit/pre-commit)
[![cici-tools enabled](https://img.shields.io/badge/%E2%9A%A1_cici--tools-enabled-c0ff33)](https://gitlab.com/buildgarden/tools/cici-tools)
[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg)](https://github.com/prettier/prettier)

<!-- END BADGIE TIME -->

Create project scaffolding for OpenTofu projects.

> Do not use this software unless you are an active collaborator on the
> associated research project.
>
> This project is an output of an ongoing, active research project. It is
> published without warranty, is subject to change at any time, and has not been
> certified, tested, assessed, or otherwise assured of safety by any person or
> organization. Use at your own risk.

## Why use Tofufy

Tofufy rapidly configures your GitLab project with everything you need to get
started with a OpenTofu managed project.

## Installation

**_Note_**: Tofufy is wrtten in Go. Make sure [Go is
installed](https://go.dev/doc/install) before proceeding.

Building from source is currently the only available installation option.
Navigate to the [Tofufy project](https://gitlab.com/buildgarden/tools/tofufy)
and checkout the project. Once checked out, from the root directory run the
following:

```bash
go build
go install
```

## Usage

Navigate to the root directory of the GitLab project you will build your
OpenTofu deployment from. Run:

```bash
tofufy
```

The following output should be displayed after running `tofufy` command:

```bash
tofufy -d DEPLOYMENT [-e ENVIRONMENT ...] [-p PROVIDER ...]
  -c string
    	copyright text
  -d string
    	name of deployment
  -e value
    	name of environment
  -i	initialize project after generation
  -l string
    	license
  -p value
    	name of provider
```

Below is an example running `tofufy` command passing all flag options:

```bash
tofufy \
    -c '2024 UL Research Institutes' \
    -d gcp-organization \
    -e production -e staging \
    -i \
    -l Apache-2.0 \
    -p cloudflare:cloudflare/cloudflare
```

## Tofufy flag options

The GitLab project ID is inferred from the Git remote URL. Only `gitlab.com` is
currently available.

### `-c TEXT`

Add copyright text for license configuration and generate `NOTICE` file.

### `-d DEPLOYMENT`

Specify deployment name.

```bash
tofufy -d app
```

### `-e ENVIRONMENT`

Specify names of environments to create. May be passed multiple times.

```bash
tofufy -e staging -e production
```

### `-l LICENSE`

Adds SPDX-ID of license to include in the project abd will generate a `LICENSE`
file with the appropriate information.

### `-p PROVIDER`

Creates the `locals.tf` file containing the [OpenTofu
providers](https://developer.hashicorp.com/terraform/language/providers) passed
with the `-p` flag. Referencing the example directly below, it will create a
`locals.tf` file containing the `cloudflare` OpenTofu provider. Delimite string
with colons (`:`) to pass provider name and source. The version is automatically
constrained to the latest provider release.

```bash
tofufy -p 'cloudflare:cloudflare/cloudflare'
```

## Configuration

Create an `env.sh` file and populate the following variables:

```bash
# GitLab access token to access the state backend
export GITLAB_USERNAME=""
export GITLAB_PASSWORD=""

# GitHub access token for querying provider packages
export GITHUB_TOKEN=""
```

Source the `env.sh` file:

```bash
. env.sh
```

## What will Tofufy add to my project?

When Tofufy is run, your project from the root directory should look similar to
the following:

```bash
$ tree
.
├── deploy
│   ├── production
│   │   ├── main.tf
│   │   └── terraform.tf
│   └── staging
│       ├── main.tf
│       └── terraform.tf
├── LICENSE
├── locals.tf
├── NOTICE
├── terraform.tf
└── variables.tf
```

When Tofufy is run in an empty project, it adds a `deploy` directory with
environment sub-directories. In our case, we want both `production` and
`staging` because we want to deploy to both the staging and production
environments.

Inside the `production` and `staging` directories, there should be a `main.tf`
and a `terraform.tf`. The `terraform.tf` will contain information to communicate
with the `staging`/`production` OpenTofu state files in the GitLab project. The
`main.tf` will have a root module with the environment set. We will build this
file out more as we write our OpenTofu code for anything specific to the
production or staging environments, but this will get us started.

In the root directory, you should have a `LICENSE` file, a `locals.tf`, a
`NOTICE`, a `terraform.tf` and a `variables.tf` to get the project started.
