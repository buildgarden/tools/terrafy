// SPDX-FileCopyrightText: 2024 UL Research Institutes
// SPDX-License-Identifier: MIT

package main

import (
	"fmt"
	"log"
	"os/exec"
)

func initDetectSecrets(path string) {
	fmt.Println("initializing detect-secrets ...")
	cmd := exec.Command("/bin/sh", "-c", "detect-secrets scan > .secrets.baseline")
	cmd.Dir = path
	err := cmd.Run()
	if err != nil {
		log.Fatal(err)
	}
}
