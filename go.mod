module gitlab.com/buildgarden/tools/tofufy

go 1.21.1

replace github.com/hashicorp/terraform-exec => github.com/opentofu/tofu-exec v0.0.0-20240530135130-cf3d41b2300e

require (
	github.com/hashicorp/hcl/v2 v2.19.1
	github.com/hashicorp/terraform-exec v0.20.0
	github.com/zclconf/go-cty v1.14.4
	gopkg.in/yaml.v3 v3.0.1
)

require (
	github.com/agext/levenshtein v1.2.1 // indirect
	github.com/apparentlymart/go-textseg/v15 v15.0.0 // indirect
	github.com/google/go-cmp v0.6.0 // indirect
	github.com/hashicorp/go-version v1.6.0 // indirect
	github.com/hashicorp/terraform-json v0.22.1 // indirect
	github.com/mitchellh/go-wordwrap v0.0.0-20150314170334-ad45545899c7 // indirect
	golang.org/x/text v0.14.0 // indirect
)
