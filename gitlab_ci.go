// SPDX-FileCopyrightText: 2024 UL Research Institutes
// SPDX-License-Identifier: MIT

package main

import (
	"fmt"
)

type GitLabCIInclude struct {
	Project string   `yaml:"project"`
	Ref     string   `yaml:"ref,omitempty"`
	File    []string `yaml:"file"`
}

type GitLabCIFile struct {
	Stages   []string          `yaml:"stages,omitempty"`
	Includes []GitLabCIInclude `yaml:"include,omitempty"`
}

func writeGitLabCIFile(filename string, environments []string) {
	jobs := []string{
		"opentofu-fmt.yml",
	}

	for _, environment := range environments {
		jobs = append(jobs, []string{
			fmt.Sprintf("opentofu-%s-apply.yml", environment),
			fmt.Sprintf("opentofu-%s-plan.yml", environment),
			fmt.Sprintf("opentofu-%s-validate.yml", environment),
			fmt.Sprintf("opentofu-%s-trivy.yml", environment),
		}...)
	}

	ciFile := GitLabCIFile{
		Stages: []string{"test", "build", "deploy"},
		Includes: []GitLabCIInclude{
			{
				Project: "buildgarden/pipelines/detect-secrets",
				File:    []string{"detect-secrets.yml"},
			},
			{
				Project: "buildgarden/pipelines/prettier",
				File:    []string{"prettier.yml"},
			},
			{
				Project: "buildgarden/pipelines/opentofu",
				File:    jobs,
			},
		},
	}

	writeFile(filename, encodeYaml(ciFile))
}
