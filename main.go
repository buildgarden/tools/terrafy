// SPDX-FileCopyrightText: 2024 UL Research Institutes
// SPDX-License-Identifier: MIT

package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/url"
	"os"
	"os/exec"
	"path/filepath"
	"regexp"
	"strings"

	"github.com/hashicorp/hcl/v2"
	"github.com/hashicorp/hcl/v2/hclwrite"
)

func addVariable(body *hclwrite.Body, name string, kind string) {
	block := body.AppendNewBlock("variable", []string{name})
	blockBody := block.Body()
	blockBody.SetAttributeTraversal("type", hcl.Traversal{
		hcl.TraverseRoot{
			Name: kind,
		},
	})
}

func writeVariablesFile(filename string) {
	file := hclwrite.NewEmptyFile()
	rootBody := file.Body()

	addVariable(rootBody, "environment", "string")
	writeFile(filename, string(file.Bytes()))
}

type Project struct {
	Id                int    `json:"id"`
	Name              string `json:"name"`
	Path              string `json:"path"`
	PathWithNamespace string `json:"path_with_namespace"`
	DefaultBranch     string `json:"default_branch"`
	WebUrl            string `json:"web_url"`
}

type ProjectRemote struct {
	Name string
	Url  string
}

func getProjectRemote(line string) ProjectRemote {
	parts := strings.Split(line, "\t")
	remote := ProjectRemote{
		Name: parts[0],
	}
	parts = strings.Split(parts[1], " ")
	remote.Url = parts[0]
	return remote
}

func getProjectRemotes() map[string]string {
	out, err := exec.Command("git", "remote", "-v").Output()
	if err != nil {
		log.Fatal(err)
	}

	remotes := make(map[string]string)
	for _, line := range strings.Split(string(out), "\n") {
		if len(line) > 0 {
			remote := getProjectRemote(line)
			if _, found := remotes[remote.Name]; !found {
				remotes[remote.Name] = remote.Url
			}
		}
	}
	return remotes
}

var reGitRepo = regexp.MustCompile(`(?P<user>git)@(?P<host>.*?):(?P<path>.*?)\.git`)

func main() {
	flag.StringVar(&copyright, "c", "", "copyright text")
	flag.StringVar(&license, "l", "", "license")
	flag.StringVar(&deployment, "d", "", "name of deployment")
	flag.Var(&environments, "e", "name of environment")
	flag.Var(&providers, "p", "name of provider")
	flag.BoolVar(&initEnabled, "i", false, "initialize project after generation")
	flag.Parse()

	if deployment == "" {
		fmt.Printf("tofufy -d DEPLOYMENT [-e ENVIRONMENT ...] [-p PROVIDER ...]\n\n")
		flag.PrintDefaults()
		os.Exit(1)
	}

	remotes := getProjectRemotes()

	fmt.Printf("remote: %s\n", remotes["origin"])
	full_path := reGitRepo.FindStringSubmatch(remotes["origin"])[3]

	projectUrl := "https://gitlab.com/api/v4/projects/" + url.QueryEscape(full_path)

	client := &http.Client{}

	request, err := http.NewRequest("GET", projectUrl, nil)
	if err != nil {
		log.Fatal(err)
	}

	token := os.Getenv("GITLAB_TOKEN")
	if len(token) > 0 {
		request.Header.Add("PRIVATE-TOKEN", token)
	}

	response, err := client.Do(request)
	if err != nil {
		log.Fatal(err)
	}

	defer response.Body.Close()

	body, err := io.ReadAll(response.Body)
	if err != nil {
		log.Fatal(err)
	}

	var project Project
	err = json.Unmarshal(body, &project)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("\nproject ID: %d\n", project.Id)
	fmt.Printf("project path: %s\n", project.PathWithNamespace)
	fmt.Printf("deployment: %s\n", deployment)
	fmt.Printf("environments: %s\n", environments)
	fmt.Printf("license: %s\n", license)
	fmt.Printf("copyright: '%s'\n", copyright)
	fmt.Printf("providers: %s\n\n", providers)

	root_path, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}

	writeRootModule(project, root_path)
	writeGitLabCIFile(filepath.Join(root_path, ".gitlab-ci.yml"), environments)
	writeLicenseEyeConfig(filepath.Join(root_path, ".licenserc.yaml"))
	for _, environment := range environments {
		workspace_path := filepath.Join(root_path, "deploy", environment)
		writeTerraformWorkspace(workspace_path, environment, project)
	}

	if initEnabled {
		initDetectSecrets(root_path)
		initPreCommitConfig(root_path)
		for _, environment := range environments {
			workspace_path := filepath.Join(root_path, "deploy", environment)
			initTerraformWorkspace(workspace_path)
		}
	}
}
