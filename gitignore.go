// SPDX-FileCopyrightText: 2024 UL Research Institutes
// SPDX-License-Identifier: MIT

package main

import "sort"

var gitignoreEntries = []string{
	"*.tfstate*",
	"*.tfvars",
	".terraform/",
	"plan.json",
	"init.sh",
	"env.sh",
	"node_modules/",
}

func writeGitignoreFile(filename string) {
	var output string
	sort.Strings(gitignoreEntries)
	for _, line := range gitignoreEntries {
		output += line + "\n"
	}

	writeFile(filename, output)
}
