// SPDX-FileCopyrightText: 2024 UL Research Institutes
// SPDX-License-Identifier: MIT

package main

import (
	"fmt"
	"log"
	"os/exec"
)

type PreCommitHook struct {
	Id   string   `yaml:"id"`
	Args []string `yaml:"args,omitempty,flow"`
}

type PreCommitRepo struct {
	Repo  string          `yaml:"repo"`
	Rev   string          `yaml:"rev"`
	Hooks []PreCommitHook `yaml:"hooks"`
}

type PreCommitConfig struct {
	Repos []PreCommitRepo `yaml:"repos"`
}

func writePreCommitFile(filename string) {
	config := PreCommitConfig{
		Repos: []PreCommitRepo{
			{
				Repo: "https://github.com/pre-commit/pre-commit-hooks",
				Rev:  "",
				Hooks: []PreCommitHook{
					{Id: "check-added-large-files"},
					{Id: "check-merge-conflict"},
					{Id: "end-of-file-fixer"},
					{Id: "fix-byte-order-marker"},
					{Id: "trailing-whitespace"},
				},
			},
			{
				Repo: "https://gitlab.com/buildgarden/pipelines/prettier",
				Rev:  "",
				Hooks: []PreCommitHook{
					{Id: "prettier"},
				},
			},
			{
				Repo: "https://gitlab.com/buildgarden/pipelines/skywalking-eyes",
				Rev:  "",
				Hooks: []PreCommitHook{
					{Id: "license-eye-header-fix"},
				},
			},
			{
				Repo: "https://gitlab.com/buildgarden/tools/badgie",
				Rev:  "",
				Hooks: []PreCommitHook{
					{Id: "badgie"},
				},
			},
			{
				Repo: "https://gitlab.com/buildgarden/tools/cici-tools",
				Rev:  "",
				Hooks: []PreCommitHook{
					{Id: "cici-update"},
				},
			},
			{
				Repo: "https://gitlab.com/buildgarden/pipelines/opentofu",
				Rev:  "",
				Hooks: []PreCommitHook{
					{Id: "opentofu-fmt"},
					{Id: "opentofu-trivy"},
					{Id: "opentofu-validate"},
				},
			},
			{
				Repo: "https://gitlab.com/buildgarden/pipelines/detect-secrets",
				Rev:  "",
				Hooks: []PreCommitHook{{
					Id: "detect-secrets",
				}},
			},
		},
	}

	writeFile(filename, encodeYaml(config))
}

func initPreCommitConfig(path string) {
	fmt.Println("initializing pre-commit ...")
	cmd := exec.Command("pre-commit", "install")
	cmd.Dir = path
	err := cmd.Run()
	if err != nil {
		log.Fatal(err)
	}

	cmd = exec.Command("pre-commit", "autoupdate")
	cmd.Dir = path
	err = cmd.Run()
	if err != nil {
		log.Fatal(err)
	}
}
