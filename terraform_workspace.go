// SPDX-FileCopyrightText: 2024 UL Research Institutes
// SPDX-License-Identifier: MIT

package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strconv"

	"github.com/hashicorp/hcl/v2/hclwrite"
	"github.com/hashicorp/terraform-exec/tfexec"
	"github.com/zclconf/go-cty/cty"
)

func writeWorkspaceTerraformFile(filename string, environment string, project Project) {

	file := hclwrite.NewEmptyFile()
	rootBody := file.Body()

	tfBlock := rootBody.AppendNewBlock("terraform", nil)
	tfBody := tfBlock.Body()

	backendBlock := tfBody.AppendNewBlock("backend", []string{"http"})
	backendBody := backendBlock.Body()

	stateUrl := "https://gitlab.com/api/v4/projects/" + strconv.Itoa(project.Id) + "/terraform/state/" + environment
	stateLockUrl := stateUrl + "/lock"

	backendBody.SetAttributeValue("address", cty.StringVal(stateUrl))
	backendBody.SetAttributeValue("lock_address", cty.StringVal(stateLockUrl))
	backendBody.SetAttributeValue("unlock_address", cty.StringVal(stateLockUrl))
	backendBody.SetAttributeValue("lock_method", cty.StringVal("POST"))
	backendBody.SetAttributeValue("unlock_method", cty.StringVal("DELETE"))
	backendBody.SetAttributeValue("retry_wait_min", cty.StringVal("5"))

	writeFile(filename, string(file.Bytes()))
}

func writeWorkspaceMainFile(filename string, environment string) {
	file := hclwrite.NewEmptyFile()
	rootBody := file.Body()

	module := rootBody.AppendNewBlock("module", []string{"root"})
	moduleBody := module.Body()
	moduleBody.SetAttributeValue("source", cty.StringVal("../.."))
	moduleBody.AppendNewline()
	moduleBody.SetAttributeValue("environment", cty.StringVal(environment))

	writeFile(filename, string(file.Bytes()))
}

func writeTerraformWorkspace(path string, environment string, project Project) {
	writeWorkspaceTerraformFile(filepath.Join(path, "terraform.tf"), environment, project)
	writeWorkspaceMainFile(filepath.Join(path, "main.tf"), environment)
}

func initTerraformWorkspace(path string) {
	fmt.Println("initializing opentofu ...")
	tf, err := tfexec.NewTerraform(path, "tofu")
	if err != nil {
		log.Fatalf("error running NewTerraform: %s", err)
	}

	err = tf.Init(
		context.Background(),
		tfexec.Upgrade(true),
		tfexec.BackendConfig("username="+os.Getenv("GITLAB_USERNAME")),
		tfexec.BackendConfig("password="+os.Getenv("GITLAB_PASSWORD")),
	)
	if err != nil {
		log.Fatalf("error running Init: %s", err)
	}
}
