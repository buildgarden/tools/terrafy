// SPDX-FileCopyrightText: 2024 UL Research Institutes
// SPDX-License-Identifier: MIT

package main

import (
	"encoding/json"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"sort"
	"strings"

	"github.com/hashicorp/hcl/v2"
	"github.com/hashicorp/hcl/v2/hclsyntax"
	"github.com/hashicorp/hcl/v2/hclwrite"
	"github.com/zclconf/go-cty/cty"
)

type GitHubRelease struct {
	TagName string `json:"tag_name"`
}

type Provider struct {
	Name    string
	Source  string
	Version string
}

func getProviderVersion(source string) string {
	parts := strings.Split(source, "/")
	namespace := parts[0]
	name := parts[1]

	projectUrl := "https://api.github.com/repos/" + namespace + "/terraform-provider-" + name + "/releases/latest"

	client := &http.Client{}

	request, err := http.NewRequest("GET", projectUrl, nil)
	if err != nil {
		log.Fatal(err)
	}

	token := os.Getenv("GITHUB_TOKEN")
	if len(token) > 0 {
		request.Header.Add("Authorization", "Bearer "+token)
	}

	response, err := client.Do(request)
	if err != nil {
		log.Fatal(err)
	}

	defer response.Body.Close()

	var release GitHubRelease
	err = json.NewDecoder(response.Body).Decode(&release)
	if err != nil {
		log.Fatal(err)
	}

	return "~> " + strings.ReplaceAll(release.TagName, "v", "")
}

func writeRootTerraformFile(filename string) {
	file := hclwrite.NewEmptyFile()
	rootBody := file.Body()

	tfBlock := rootBody.AppendNewBlock("terraform", nil)
	tfBody := tfBlock.Body()

	providersMap := make(map[string]map[string]cty.Value)
	for _, value := range providers {
		parts := strings.Split(value, ":")
		name := parts[0]
		providersMap[name] = make(map[string]cty.Value)
		providersMap[name]["source"] = cty.StringVal(parts[1])
		providersMap[name]["version"] = cty.StringVal(getProviderVersion(parts[1]))
	}

	providersKeys := make([]string, 0, len(providersMap))
	for k := range providersMap {
		providersKeys = append(providersKeys, k)
	}
	sort.Strings(providersKeys)

	requiredProvidersBlock := tfBody.AppendNewBlock("required_providers", nil)
	requiredProvidersBody := requiredProvidersBlock.Body()
	for _, key := range providersKeys {
		requiredProvidersBody.SetAttributeValue(key, cty.MapVal(providersMap[key]))
	}
	tfBody.SetAttributeValue("required_version", cty.StringVal(">=1.0"))

	writeFile(filename, string(file.Bytes()))
}

func writeRootLocalsFile(project Project, filename string) {
	file := hclwrite.NewEmptyFile()
	rootBody := file.Body()

	localsBlock := rootBody.AppendNewBlock("locals", nil)
	localsBody := localsBlock.Body()
	localsBody.SetAttributeValue("deployment", cty.StringVal(deployment))

	token := hclwrite.Tokens{
		{Type: hclsyntax.TokenOQuote, Bytes: []byte(`"`)},
		{Type: hclsyntax.TokenTemplateInterp, Bytes: []byte(`${`)},
		{Type: hclsyntax.TokenIdent, Bytes: []byte(`var.environment`)},
		{Type: hclsyntax.TokenTemplateSeqEnd, Bytes: []byte(`}`)},
		{Type: hclsyntax.TokenQuotedLit, Bytes: []byte(`-`)},
		{Type: hclsyntax.TokenTemplateInterp, Bytes: []byte(`${`)},
		{Type: hclsyntax.TokenIdent, Bytes: []byte(`local.deployment`)},
		{Type: hclsyntax.TokenTemplateSeqEnd, Bytes: []byte(`}`)},
		{Type: hclsyntax.TokenCQuote, Bytes: []byte(`"`)},
	}
	localsBody.SetAttributeRaw("name", token)
	localsBody.AppendNewline()

	localsBody.SetAttributeRaw("default_tags", hclwrite.TokensForObject(
		[]hclwrite.ObjectAttrTokens{
			{
				Name: hclwrite.Tokens{
					{Type: hclsyntax.TokenStringLit, Bytes: []byte(`deployment`)},
				},
				Value: hclwrite.TokensForTraversal(hcl.Traversal{
					hcl.TraverseRoot{
						Name: "local",
					},
					hcl.TraverseAttr{
						Name: "deployment",
					},
				}),
			},
			{
				Name: hclwrite.Tokens{
					{Type: hclsyntax.TokenStringLit, Bytes: []byte(`environment`)},
				},
				Value: hclwrite.TokensForTraversal(hcl.Traversal{
					hcl.TraverseRoot{
						Name: "var",
					},
					hcl.TraverseAttr{
						Name: "environment",
					},
				}),
			},
		},
	))

	writeFile(filename, string(file.Bytes()))
}

func writeRootModule(project Project, path string) {
	writeGitignoreFile(filepath.Join(path, ".gitignore"))
	writePreCommitFile(filepath.Join(path, ".pre-commit-config.yaml"))
	writeRootTerraformFile(filepath.Join(path, "terraform.tf"))
	writeRootLocalsFile(project, filepath.Join("locals.tf"))
	writeVariablesFile(filepath.Join("variables.tf"))
}
