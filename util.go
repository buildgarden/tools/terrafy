// SPDX-FileCopyrightText: 2024 UL Research Institutes
// SPDX-License-Identifier: MIT

package main

import (
	"bytes"
	"fmt"
	"log"
	"os"
	"path/filepath"

	"gopkg.in/yaml.v3"
)

func encodeYaml(data interface{}) string {
	var buffer bytes.Buffer
	yamlEncoder := yaml.NewEncoder(&buffer)
	yamlEncoder.SetIndent(2)
	err := yamlEncoder.Encode(&data)
	if err != nil {
		log.Fatalf("error: %v", err)
	}
	return buffer.String()
}

func writeFile(filename string, text string) {
	current_path, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}

	full_path, err := filepath.Abs(filename)
	if err != nil {
		log.Fatal(err)
	}

	rel_path, err := filepath.Rel(current_path, full_path)
	if err != nil {
		log.Fatal(err)
	}

	rel_dir_path := filepath.Dir(rel_path)

	err = os.MkdirAll(rel_dir_path, 0755)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("writing " + rel_path)

	file, err := os.Create(full_path)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	file.WriteString(text)
}
